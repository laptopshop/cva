import { Injectable } from '@angular/core';
import { LoopbackService } from '../../_services/loopback/loopback.service'

import { Entity } from '../../_interfaces/product'
import { Product } from '../../_interfaces/cva'
import { resolve } from 'q';

@Injectable({
	providedIn: 'root'
})

export class Extraction {

	groupId: number;
	products: Array<any>;
	
	constructor(
		private loopbackService: LoopbackService,
		) { }
	// -----------------------------------------------------------------------------------------------

	public asyncForEach = async (array: Array<any>,  callback) => {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array)
		}
	}

	//------------------------------------------------------------------------------------------------

	public categorizeProducts = async (groupId: number, products: Array<Product>) => {
		this.groupId = groupId;
		this.products = products;
		var productsToAdd = [];
		var productsToUpdate = [];
		return await this.asyncForEach(products, async (product, index) => {
			await new Promise((resolve, reject) => {
				this.loopbackService.checkProductEntity(product.clave).subscribe(
					(response: Array<Entity>) => {
						if(response.length == 0) productsToAdd.push(index);
						else if(response.length > 0) productsToUpdate.push({magentoProd:response[0],cvaPosition:index});
						resolve({'notExist':productsToAdd, 'exist':productsToUpdate});
					},
					error => { reject(error); }
				);
			});
		}).then(()=>{
			return({'notExist':productsToAdd, 'exist':productsToUpdate});
		})
	}

	//------------------------------------------------------------------------------------------------
	
	public addProduct(position: number) : Promise<any> {
		return new Promise((resolve, reject) => {
			this.loopbackService.newProductEntity(this.products[position].clave).toPromise()
			.then(
				response => { resolve({'magento':response['entity_id'], 'cva':this.products[position].clave}); },
				err => { reject(err); }
			);
		});
	}
		
	public addProductEntities(entity_id:string, position: number)  {
		
		var title = this.products[position].descripcion;
		var titleUrl = title.toLowerCase().replace(/\W+/g, '-');
		if(this.products[position]['ficha_tecnica'].length > 254) this.products[position]['ficha_tecnica'] = this.products[position]['ficha_tecnica'].substring(0,254);
		
		var _name				= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,73,title).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _metatitle			= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,84,title).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _metadescription	= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,86,title).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _urlkey				= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,126,titleUrl).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _optionscontainer	= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,106,'container2').toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _marca				= new Promise((resolve,reject) => { this.loopbackService.newProductEntityVarchar(entity_id,161,this.products[position].marca).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		var _website			= new Promise((resolve,reject) => { this.loopbackService.newWebsite(entity_id).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _price				= new Promise((resolve,reject) => { this.loopbackService.newProductEntityDecimal(entity_id,77,this.products[position].precio).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _giftavailable		= new Promise((resolve,reject) => { this.loopbackService.newProductEntityDecimal(entity_id,133,'1.0000').toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		var _status				= new Promise((resolve,reject) => { this.loopbackService.newProductEntityInt(entity_id,97,0).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _visibility			= new Promise((resolve,reject) => { this.loopbackService.newProductEntityInt(entity_id,99,4).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _taxClass			= new Promise((resolve,reject) => { this.loopbackService.newProductEntityInt(entity_id,132,2).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _importedBy			= new Promise((resolve,reject) => { this.loopbackService.newProductEntityInt(entity_id,162,214).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		var _stockLimit			= new Promise((resolve,reject) => { this.loopbackService.newCIStockItems(entity_id,this.products[position].disponible).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _stock				= new Promise((resolve,reject) => { this.loopbackService.newCIStockStatus(entity_id,this.products[position].disponible).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _stockStatus		= new Promise((resolve,reject) => { this.loopbackService.newProductEntityInt(entity_id,115,1).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _indexEAV			= new Promise((resolve,reject) => { this.loopbackService.newIndexEAV(entity_id).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });

		
		var _searchFulltext		= new Promise((resolve,reject) => { this.loopbackService.newCSFulltextScope1(entity_id,73,title).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _category			= new Promise((resolve,reject) => { this.loopbackService.newCatalogCategoryProduct(entity_id,this.groupId).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });

		var _productUrlRewrite	= new Promise((resolve,reject) => { this.loopbackService.newUrlRewrite({
				"entity_id": entity_id,
				"request_path": titleUrl + '.html',
				"target_path": "catalog/product/view/id/" + entity_id + "/",
		}).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		var _categoryUrlRewrite	= new Promise((resolve,reject) => { this.loopbackService.newUrlRewrite({
				"entity_id": entity_id,
				"request_path": titleUrl + '.html',
				"target_path": "catalog/product/view/id/" + entity_id + "/category/" + this.groupId + "/",
				"metadata" : '"{\"category_id\":\"' + this.groupId + '\"}"'
		}).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });

		var _storeUrlRewrite	= new Promise((resolve,reject) => { this.loopbackService.newUrlRewrite({
				"entity_id": entity_id,
				"request_path": '/' + titleUrl + '.html',
				"target_path": "catalog/product/view/id/" + entity_id + "/category/" + this.groupId + "/",
				"metadata" : '"{\"category_id\":\"2\"}"'
		}).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		var _indexPriceG1		= new Promise((resolve,reject) => { this.loopbackService.newCatalogProductIndexPrice(entity_id,0,this.products[position].precio).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _indexPriceG2		= new Promise((resolve,reject) => { this.loopbackService.newCatalogProductIndexPrice(entity_id,1,this.products[position].precio).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _indexPriceG3		= new Promise((resolve,reject) => { this.loopbackService.newCatalogProductIndexPrice(entity_id,2,this.products[position].precio).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		var _indexPriceG4		= new Promise((resolve,reject) => { this.loopbackService.newCatalogProductIndexPrice(entity_id,3,this.products[position].precio).toPromise().then( () => {resolve();}, (err)=> {reject(err);} ); });
		
		// this.loopbackService.newCatalogProductMediaGallery(this.products[position].imagen).subscribe(
		// 	response => {
		// 		console.log(response);
		// 		this.loopbackService.newCatalogProductMediaGalleryValue(response['value_id'], entity_id).subscribe()
		// 	}
		// );
		
		return Promise.all([
			_name,
			_metatitle,
			_metadescription,
			_urlkey,
			_optionscontainer,
			_marca,
			_website,
			_price,
			_giftavailable,
			_status,
			_visibility,
			_taxClass,
			_stockStatus,
			_importedBy,
			_stockLimit,
			_stock,
			_productUrlRewrite,
			_categoryUrlRewrite,
			_storeUrlRewrite,
			_indexEAV,
			_searchFulltext,
			_category,
			_indexPriceG1,
			_indexPriceG2,
			_indexPriceG3,
			_indexPriceG4
		]);
		
	}
	
	//------------------------------------------------------------------------------------------------
	
	public updateProductEntities(product: Entity, position: number) {

		var title = this.products[position].descripcion;
		var titleUrl = title.toLowerCase().replace(/\W+/g, '-');
		var dataUrl = {
			"entity_id": product.entity_id,
			"request_path": titleUrl,
			"target_path": "catalog/product/view/id/" + product.entity_id
		}
						
		var _decimalPrice = new Promise((resolve, reject) => {
			this.loopbackService.updateProductEntityDecimal(
				product.entity_id,
				77,
				this.products[position].precio
			).toPromise().then(
				(response:Entity) => { resolve(response);},
				err => { reject(err); }
			);
		});

		var _indexPrice = new Promise((resolve, reject) => {
			this.loopbackService.updateCatalogProductIndexPrice(
				product.entity_id,
				this.products[position].precio
			).toPromise().then(
				(response:Entity) => { resolve(response);},
				err => { reject(err); }
			);
		});

		var _stock = new Promise((resolve, reject) => {
			this.loopbackService.updateCIStockStatus(
				product.entity_id,
				this.products[position].disponible
			).toPromise().then(
				(response:Entity) => { resolve(response);},
				err => { reject(err); }
			);
		});

		var _stockLimit = new Promise((resolve, reject) => {
			this.loopbackService.updateCIStockItems(
				product.entity_id,
				this.products[position].disponible
			).toPromise().then(
				(response:Entity) => { resolve(response);},
				err => { reject(err); }
			);
		});

		// var _category	= new Promise((resolve,reject) => { this.loopbackService.updateCatalogCategoryProduct(product.entity_id,this.groupId).toPromise().then( () => { resolve();},err => { reject(err); });});
		// var _status		= new Promise((resolve,reject) => { this.loopbackService.updateProductEntityInt(product.entity_id,97,0).toPromise().then( () => { resolve();},err => { reject(err); });});
		// var _urlRewrite	= new Promise((resolve,reject) => { this.loopbackService.updateUrlRewrite(dataUrl).toPromise().then( () => { resolve();},err => { reject(err); });});

		return Promise.all([
			_decimalPrice,
			_indexPrice,
			_stock,
			_stockLimit,
			// _category,
			// _status,
			// _urlRewrite
		]);
		
	}
}
