import { Injectable } from '@angular/core';

import { Extraction } from '../../_classes/products/extraction'
import { Cva } from '../../_classes/cva/cva'

import { Product } from '../../_interfaces/cva'
import { Group } from 'src/app/_interfaces/general';

export interface categorizedProducts {
	"exist": Array<number>;
	"notExist": Array<any>;
}


@Injectable({
	providedIn: 'root'
})

export class Usage {

	products: Array<Product>;

	constructor(
		private productsExtraction: Extraction,
		private cva: Cva
		) { }

	// ------------------------------------------------------------------------------------------------------------

	public getGroups () {
		return new Promise((resolve, reject) => {
			this.cva.getGroups().then((groups:Array<Group>) => {
				resolve(groups);
			});
		})
	}

	// ------------------------------------------------------------------------------------------------------------

	public asyncForEach = async (array: Array<any>,  callback) => {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array)
		}
	}

	// ------------------------------------------------------------------------------------------------------------

	public addProducts = async (products) => {
		await this.asyncForEach(products, async (product, position, arr) => {
			await new Promise((resolve, reject) => {
				this.productsExtraction.addProduct(product)
				.then((product: string) => {
					this.productsExtraction.addProductEntities(product['magento'], position);
					console.log("  + " + product['cva']);
					resolve();
				}).catch((err)=>{
					reject(err);
				});
			});
		})
	}

	public updateProducts = async (products) => {
		await this.asyncForEach(products, async (product) => {
			await new Promise((resolve, reject) => {
				this.productsExtraction.updateProductEntities(
					product['magentoProd'],
					product['cvaPosition'],
				).then(()=>{
					resolve();
				}).catch((err)=>{
					reject(err);
				});
			});
		})
	}

	// ------------------------------------------------------------------------------------------------------------

	public importCategoryDataByGroup(group:Group) {
		return new Promise((resolve, reject) => {
			console.log("2 - Getting xmlResponse");
			this.cva.getXmlResponse(group).then((xmlResponse:string)=>{
				console.log("1 - Formatting Products");
				this.cva.formatXmlToJson(xmlResponse).then((products:Array<any>)=>{						
					this.products = products;
					resolve({
						group: group,
						products: products
					});
				}).catch((err)=>{
					reject(err);
				});
			});
		});
	}

	public importCategoryDataByGroupName(groupName:string) {
		return new Promise((resolve, reject) => {
			console.log("3 - Getting Group Data");
			this.cva.getGroup(groupName).then((group:Group)=>{
				this.importCategoryDataByGroup(group).then(response=>{
					resolve(response);
				}).catch((err)=>{
					reject(err);
				});
			});
		});
	}

	public importCategoryProducts(magentoCategory:number) {
		return new Promise((resolve,reject) => {
			this.productsExtraction.categorizeProducts(magentoCategory, this.products).then((products)=>{
				console.log(">>> Se actualizarán ", products['exist'].length + " productos.");
				this.updateProducts(products['exist']).then(() => {
					console.log('✓ - Actualización Completa ');
					console.log(">>> Se agregarán " + products['notExist'].length + " productos.");
					this.addProducts(products['notExist']).then(() => {
						console.log('✓ - Adiciones Añadidas ');
						resolve();
					});
				}).catch((err)=>{
					reject(err);
				});
			});
		});
	}

}
