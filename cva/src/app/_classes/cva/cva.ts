import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Group } from '../../_interfaces/general'
import { reject } from 'q';
import { Product } from 'src/app/_interfaces/cva';

@Injectable({
	providedIn: 'root'
})

export class Cva {
	
	revenue: number;
	
	constructor(
		private http: HttpClient,
		private ngxXml2jsonService: NgxXml2jsonService
		) { }

	public getGroup(name: string) {
		return new Promise((resolve, reject) => {
			this.http.get('assets/grupos.json').subscribe(
				(response: Array<any>) => {
					const group = response.find((group) => { return group.cvaId == name; });
					resolve(group);
				}, err => { reject(err); }
			);
		});
	}

	public getGroups() {
		return new Promise((resolve, reject) => {
			this.http.get('assets/grupos.json').subscribe(
				(groups: Array<Group>) => { resolve(groups); },
				(err) => { reject(err); }
			);
		});
	}
	
	public getXmlResponse(group:Group) {
		this.revenue = group['revenue'];
		return new Promise((resolve, reject) => {
			// const url = "https://www.grupocva.com/catalogo_clientes_xml/lista_precios.xml?cliente=19156&marca=%&grupo=" + group['cvaId'] + "&codigo=%&porcentaje=" + (group['revenue'] + iva) + "&depto=B&MonedaPesos=1&dt=1&dc=1";
			const url = "https://www.grupocva.com/catalogo_clientes_xml/lista_precios.xml?cliente=19156&marca=%&grupo=" + group['cvaId'] + "&codigo=%&depto=B&MonedaPesos=1&dt=1&dc=1";
			this.http.get(url, { responseType: 'text' }).subscribe(
				response => { resolve(response); }, 
				err => { reject(err); }
			);
		});
	}
	
	public formatXmlToJson(xmlResponse: string) : Promise<Array<Product>> {
		const headers = new HttpHeaders({ 'Content-Type': 'text/xml' });
		headers.append('Connection:', 'Keep-Alive');
		return new Promise((resolve,reject) => {
			if(xmlResponse[0] != "<" ) reject("SERVICIO DE CVA NO DISPONIBLE");
				const parser = new DOMParser();
				const xml = parser.parseFromString(xmlResponse, 'text/xml');
				const jsonResponse = this.ngxXml2jsonService.xmlToJson(xml);
				var products: Array<Product> = new Array();
				if(jsonResponse['articulos']) {
					if(typeof (jsonResponse['articulos']['item'][0]) == "undefined"){
						if(typeof (jsonResponse['articulos']['item'].marca) != "string") jsonResponse['articulos']['item'].marca = "N/A";
						jsonResponse['articulos']['item'].costo = jsonResponse['articulos']['item'].precio;
						jsonResponse['articulos']['item'].precio = Math.ceil((jsonResponse['articulos']['item'].costo  * 11600) / (100 - this.revenue)) / 100;
						products.push(jsonResponse['articulos']['item']);
						resolve(products);
					} else {
						products = jsonResponse['articulos']['item'].map(item => {
							if(typeof (item.marca) != "string") item.marca = "N/A";
							item.costo = item.precio;
							item.precio = Math.ceil((item.costo * 11600) / (100 - this.revenue)) / 100;
							return item;
						});
						resolve(products);
					}
				}
		});
	}
}

