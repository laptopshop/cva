import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupsComponent } from './_components/groups/groups.component';
import { ProductsComponent } from './_components/products/products.component';
import { ImportComponent } from './_components/import/import.component';

const routes: Routes = [
  {
    path: 'groups',
    component: GroupsComponent,
    pathMatch: 'full'
  },
  {
    path: 'groups/:action',
    component: GroupsComponent,
    pathMatch: 'full'
  },
  {
    path: 'group/:group',
    component: ProductsComponent,
    pathMatch: 'full'
  },
  {
    path: 'import/:group',
    component: ImportComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'groups'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
