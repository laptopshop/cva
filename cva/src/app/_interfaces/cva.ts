export interface Product {
    clase: string;
    clave: string;
    codigo_fabricante: string;
    descripcion: string;
    disponible: number;
    disponibleCD: number;
    ficha_comercial: string;
    ficha_tecnica: string;
    garantia: string;
    grupo: string;
    imagen: string;
    marca: string;
    moneda: string;
    costo: number;
    precio: number;
}
