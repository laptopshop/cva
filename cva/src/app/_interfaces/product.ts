export interface Entity {
	entity_id?: number;
	attribute_set_id?: number;
	type_id?: string;
	sku: string
	has_options?: number;
	required_options?: number;
	created_at?: string;
	updated_at?: string;
	ints?: Array<EntityDecimal>;
}

export interface EntityVarchar {
	value_id?: number;
	attribute_id: number;
	store_id?: number;
	entity_id: number;
	value: string;
}

export interface Website {
	product_id: number;
	website_id: number;
}

export interface EntityDecimal {
	value_id: number;
	attribute_id: number;
	store_id: number;
	entity_id: number;
	value: number;
}

export interface IndexEAV {
	entity_id?: number;
	attribute_id?: number;
	store_id?: number;
	value?: number;
	source_id: number;
}
export interface CatalogProductIndexPrice {
	entity_id: number
	customer_group_id: number
	website_id: number
	tax_class_id: number
	price: number
	final_price: number
	min_price: number
	max_price: number
	tier_price: number
}
