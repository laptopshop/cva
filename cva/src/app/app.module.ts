import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';

import {
	MatToolbarModule, 
	MatButtonModule, 
	MatSidenavModule, 
	MatIconModule, 
	MatListModule, 
	MatFormFieldModule, 
	MatInputModule,
	MatTableModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GroupsComponent } from './_components/groups/groups.component';
import { ProductsComponent } from './_components/products/products.component';
import { ImportComponent } from './_components/import/import.component';

@NgModule({
	declarations: [
		AppComponent,
		GroupsComponent,
		ProductsComponent,
		ImportComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatFormFieldModule,
		MatInputModule,
		MatTableModule,
		BrowserAnimationsModule,
		
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
