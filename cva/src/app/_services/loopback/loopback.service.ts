import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class LoopbackService {

	server: string;	
	constructor(
		public http: HttpClient
		) {  
		this.server = "http://localhost:3000/api/";
	}

	// ADD PRODUCTS

	checkProductEntity(sku) {
		return this.http.get(this.server + 'catalog_product_entities?filter[where][sku]=' + sku);
	}

	newProductEntity(sku) {
		return this.http.post(this.server + 'catalog_product_entities', {sku: sku});
	}

	newProductEntityVarchar(sku, attr, val) {
		var entity = {
			entity_id: sku,
			attribute_id: attr,
			value: val
		}
		return this.http.post(this.server + 'catalog_product_entity_varchars', entity);
	}

	newProductEntityText(sku, attr, val) {
		var entity = {
			entity_id: sku,
			attribute_id: attr,
			value: val
		}
		return this.http.post(this.server + 'catalog_product_entity_text', entity);
	}
	
	newWebsite(entity_id) {
		return this.http.post(this.server + 'catalog_product_websites', {product_id: entity_id, website_id: 1});
	}
	
	newProductEntityDecimal(entity_id, attr, price) {
		var entity = {
			entity_id: entity_id,
			attribute_id: attr,
			value: price
		}
		return this.http.post(this.server + 'catalog_product_entity_decimals', entity);
	}

	newProductEntityInt(entity_id, attr, val) {
		var entity = {
			entity_id: entity_id,
			attribute_id: attr,
			value: val
		}
		return this.http.post(this.server + 'catalog_product_entity_ints', entity);
	}

	
	newUrlRewrite(dataUrl) {
		return this.http.post(this.server + 'url_rewrites', dataUrl);
	}
	
	newIndexEAV(entity_id) {
		return this.http.post(this.server + 'catalog_product_index_eavs', {entity_id:entity_id,source_id:entity_id});
	}
	
	newCIStockStatus(entity_id, stock) {
		return this.http.post(this.server + 'cataloginventory_stock_statuses', {product_id: entity_id,qty: stock, stock_status:1});
	}

	newCIStockItems(entity_id, stock) {
		return this.http.post(this.server + 'cataloginventory_stock_items', {product_id: entity_id,qty: stock});
	}

	newCSFulltextScope1(entity_id, attr, data){
		var scope = {
			entity_id: entity_id,
			attribute_id: attr,
			data_index: data
		}
		return this.http.post(this.server + 'catalogsearch_fulltext_scope1s', scope);
	}

	newCatalogCategoryProduct(entity_id, category_id){
		return this.http.post(this.server + 'catalog_category_products', {category_id: category_id, product_id: entity_id});
	}
	
	newCatalogProductIndexPrice(entity_id, customer_group_id, price){
		var prices = {
			"entity_id":entity_id,
			"customer_group_id":customer_group_id,
			"price":price,
			"final_price":price,
			"min_price":price,
			"max_price":price
		}
		return this.http.post(this.server + 'catalog_product_index_prices', prices);
	}

	newCatalogProductMediaGallery(image_url){
		return this.http.post(this.server + 'catalog_product_entity_media_galleries', {"value":image_url});
	}

	newCatalogProductMediaGalleryValue(value_id,entity_id){
		return this.http.post(this.server + 'catalog_product_entity_media_gallery_values', {"value_id":value_id,"entity_id":entity_id});
	}

	// UPDATE PRODUCTS

	updateProductEntityDecimal(entity_id:number, attr:number, value) {
		return this.http.post(this.server
			+ 'catalog_product_entity_decimals/update?where={"entity_id":' + entity_id + ',"attribute_id":' + attr + '}',
			{"value":value}
		);
	}

	updateCIStockStatus(entity_id:number, qty:number) {
		return this.http.post(this.server
			+ 'cataloginventory_stock_statuses/update?where={"product_id":' + entity_id + '}',
			{"qty":qty}
		);
	}

	updateCIStockItems(entity_id:number, qty:number) {
		return this.http.post(this.server
			+ 'cataloginventory_stock_items/update?where={"product_id":' + entity_id + '}',
			{"qty":qty}
		);
	}

	updateCatalogProductIndexPrice(entity_id:number, price:number){
		return this.http.post(this.server
			+ 'catalog_product_index_prices/update?where={"entity_id":' + entity_id + '}',
			{"price":price}
		);
	}

	updateCatalogCategoryProduct(entity_id, category_id){
		return this.http.post(this.server 
			+ 'catalog_category_products/upsertWithWhere?where={"product_id":' + entity_id + '}',
			{product_id:entity_id, category_id:category_id}
		);
	}

	updateProductEntityInt(entity_id, attr, val) {
		var entity = {
			entity_id: entity_id,
			attribute_id: attr,
			value: val
		}
		return this.http.post(this.server
			+ 'catalog_product_entity_ints/upsertWithWhere?where={"entity_id":' + entity_id + ',"attribute_id":' + attr + '}',
			entity
		);
	}

	updateUrlRewrite(dataUrl) {
		return this.http.post(this.server
			+ 'url_rewrites/upsertWithWhere?where={"request_path":"' + dataUrl["request_path"] + '"}',
			dataUrl);
	}

}