import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { Usage } from '../../_classes/products/usage'
import { Group } from '../../_interfaces/general'

@Component({
	selector: 'app-groups',
	templateUrl: './groups.component.html',
	styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
	
	displayedColumns: string[] = ['cvaId', 'revenue'];
	dataSource = null;
	isDataRecived = false;
	groups : Array<Group>;
	
	constructor(
		private route: ActivatedRoute,
		private productUsage: Usage
		) { }
	
	ngOnInit() {
		this.productUsage.getGroups().then((groups:Array<Group>) => {
			this.groups = groups;
			this.dataSource = new MatTableDataSource(groups);
			this.route.params.subscribe(params => {
				if(params['action'] == "import"){
					this.importProducts();
				}
			});
		}).finally(() => { this.isDataRecived = true; });
	}
	
	public applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	public importProducts() {
		this.groups.forEach(element => {
			// window.open("http://localhost:4200/import/" + element.cvaId, "_blank");
			window.open("/import/" + element.cvaId, "_blank");
		});
	}
	
}