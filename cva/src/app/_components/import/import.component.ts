import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Usage } from '../../_classes/products/usage'

@Component({
	selector: 'app-import',
	templateUrl: './import.component.html',
	styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

	status:string;

	constructor(
		private route: ActivatedRoute,
		private productUsage: Usage
		) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			console.log('%c' + params['group'], 'background: #000; color: #fff');
			this.status = "Comenzando...";
			this.productUsage.importCategoryDataByGroupName(params['group']).then((response)=> {
				this.status = "Procesando..."
				if( typeof(response['group']['magento_category']) == 'number' ){
					this.productUsage.importCategoryProducts(response['group']['magento_category'])
					.then(
						()=>{this.status = "Importación exitosa";}
					).catch(
						()=>{this.status = "Importación con errores";}
					)
				}
			});
		});
	}

}
