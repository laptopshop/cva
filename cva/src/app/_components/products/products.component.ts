import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { Usage } from '../../_classes/products/usage'

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
	
	displayedColumns = ['clave', 'descripcion', 'costo', 'precio', 'disponible', 'marca' ];
	dataSource = null;
	
	magentoCategory = 0;
	qtyResults = 0;
	cvaId = "Obteniendo Categoría";
		
	constructor(
		private route: ActivatedRoute,
		private productUsage: Usage
		) { }
	
	ngOnInit() {
		this.route.params.subscribe(params => {
			this.productUsage.importCategoryDataByGroupName(params['group']).then((response)=> {
				if(response) {
					if( typeof(response['group']['magento_category']) == 'number' ) this.magentoCategory = response['group']['magento_category'];
					this.cvaId = response['group']['cvaId'];
					this.qtyResults = response['products'].length;
					this.dataSource = new MatTableDataSource(response['products']);
				}
			});
		});
	}
	
	//------------------------------------------------------------------------------------------------
	
	public applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	public importProducts() {
		console.log('%c' + this.cvaId, 'background: #000; color: #fff');
		if(this.magentoCategory) this.productUsage.importCategoryProducts(this.magentoCategory);
		else alert('No se asignó una categoría a este grupo en Laptopshop');
	}

}